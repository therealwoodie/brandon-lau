'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var minify = require('gulp-minify');
var cleanCSS = require('gulp-clean-css');

gulp.task('sass', () => {
  return gulp.src('./assets/scss/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({ browsers: ['last 4 versions'] }))
		.pipe(cleanCSS({compatible: 'ie8'}))
		.pipe(sourcemaps.write('./maps/'))
		.pipe(gulp.dest('./dist/'));
});

gulp.task('compress', () => {
	gulp.src(['./assets/js/*.js', '!js/*.min.js'])
	  .pipe(minify())
	  .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', () => {
	gulp.watch(['./assets/scss/*.scss'], ['sass']) 
	gulp.watch(['./assets/js/*.js'], ['compress']);
});