<?
/* Template Name: Archive */
get_header();
if (have_posts()) : while (have_posts()) :
	the_post(); ?>

	<div class="articles masonry">
		<? 
			$args = array(
				'post_type' 			=> 'post',
				'posts_per_page'	=> -1
			);

			$post_query = new WP_Query($args); 
		?>

		<? if ($post_query -> have_posts()) :
			while($post_query -> have_posts()) :
				$post_query -> the_post(); ?>

				<div class="article masonry-item">
					<div class="wpr masonry-content">
						<? the_post_thumbnail(); ?>

						<div class="meta">
							<label class="date"><? $post_date = get_the_date("n/j"); echo $post_date; ?></label>
							<a href="<? the_permalink(); ?>"><? the_title(); ?></a>
							<!-- <p><? the_excerpt(); ?></p> -->
						</div>
					</div>
				</div>

			<? endwhile;
		endif; ?>

		<? wp_reset_postdata(); ?>

	</div>

<? endwhile;
endif;
get_footer(); ?>