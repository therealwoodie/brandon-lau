<?
/* Template Name: Homepage */
get_header();
if (have_posts()) : while (have_posts()) :
	the_post(); ?>

<div class="homepage">

	<div class="gallery">
		
		<!-- Start of flexible content -->
		<? if (have_rows("rows")) :
	    while (have_rows("rows")) : the_row(); ?>

			<? if (get_row_layout() == "primary_row") { ?>

				<div class="row primary">
					<div class="item large">
						<? $image = get_sub_field("large_image"); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					<div class="item medium">
						<? $image = get_sub_field("medium_image"); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					<div class="item small">
						<? $image = get_sub_field("small_image"); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>

						<? } elseif (get_row_layout() == "secondary_row") { ?>

				<div class="row secondary">
					<div class="item large">
						<? $image = get_sub_field("large_image"); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					<div class="item medium">
						<? $image = get_sub_field("medium_image"); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>

	    <? } ?>

			<? endwhile;
		endif; ?>
		<!-- End of flexible content -->

	</div><!-- ./gallery -->

	<div class="splash"><!-- scroll-section -->
		<div class="fixed-bg"><!-- scroll-bg -->

			<div class="wpr">
				<div class="logo">
					<!-- <div class="wpr"> -->
						<!-- <a href="/"><img src="/wp-content/uploads/2019/02/logo.png"></a> -->
						<img src="/wp-content/uploads/2019/02/logo.png">
					<!-- </div> -->
				</div>
			</div>
			
		</div><!-- ./fixed-bg -->

		<div class="scroll-bg"></div><!-- content -->
			
	</div><!-- ./scroll-section -->

</div><!-- ./homepage -->

<? endwhile;
endif;
get_footer(); ?>