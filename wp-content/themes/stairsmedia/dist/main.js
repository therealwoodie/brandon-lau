document.addEventListener("DOMContentLoaded", () => {
	init();

	Barba.Pjax.start();
	Barba.Prefetch.init();

	Barba.Pjax.getTransition = function() {
		return fadeTransition;
	};

	// (Not using) Barba's default page loads
	var defaultTransition = Barba.BaseTransition.extend({
		start: function() {
			this.newContainerLoading.then(this.finish.bind(this));
		},

		finish: function() {
			console.log(this);
			document.body.scrollTop = 0;
			this.done();
		}
	});

	var fadeTransition = Barba.BaseTransition.extend({
	  start: function() {
	    Promise
	      .all([this.newContainerLoading, this.fadeOut()])
	      .then(this.fadeIn.bind(this));
	  },

	  fadeOut: function() {
	    return $(this.oldContainer).animate({ opacity: 0 }).promise();

	  },

	  fadeIn: function() {
	  	$(window).scrollTop(0);

	    var _this = this;
	    var $el = $(this.newContainer);

	    $(this.oldContainer).hide();

	    $el.css({
	      visibility : 'visible',
	      opacity : 0
	    });

	    $el.animate({ opacity: 1 }, 1000, function() {
	      _this.done();
	    });
	  }
	});

	Barba.Dispatcher.on("linkClicked", (HTMLElement, MouseEvent) => {

	});

	Barba.Dispatcher.on("newPageReady", (currentStatus, oldStatus, container) => {

	});

	Barba.Dispatcher.on("transitionCompleted", (currentStatus, prevStatus) => {
		init();
	})

});

init = () => {

	// ----------
	if ($(".overlay").length) {
		$(".overlay").delay().fadeOut(1000);
	}

	// ----------
	const hamburger = document.querySelector(".hamburger");
	const responsiveNav = document.querySelector("nav");

	hamburger.addEventListener("click", function() {
		this.classList.toggle("active");

		if (this.classList.contains("active")) {
			responsiveNav.classList.add("active");
		} else {
			responsiveNav.classList.remove("active");
		}
	})

	var logoTop;

	// ----------
	$(document).on("scroll", function() {

		if ($(".splash").length) {

			var windowTop = $(document).scrollTop() | 0;
			var windowHeight = $(window).height() | 0;
			var windowBottom = windowTop + windowHeight;

			// ----------
			$(".splash").each(function() {
			  var sectionTop = $(this).offset().top;
			  var sectionBottom = sectionTop + $(this).outerHeight();

			  if (windowTop >= sectionTop) {
			    $(this).addClass("fixed");
			  } else {
			    $(this).removeClass("fixed");
			  }

			  if (windowBottom >= sectionBottom) {
			    $(this).addClass("unfixed");
			  } else {
			    $(this).removeClass("unfixed");
			  }
			}) // End of .each()

			// ----------
			var scrollableDiv = $(".splash .scroll-bg").outerHeight();
			var resizeValue = (windowTop / scrollableDiv) * 100 | 0;

			if ($(window).width() > 960) {

				if (resizeValue <= 100) {
					$(".splash .logo img").css("width", `calc(720px - ${resizeValue * 14}px)`);
					$(".splash .logo").css("transform", `translate(-50%, calc(-50% - ${resizeValue}%))`);
				}

				// ----------
				logoTop = $(".splash .logo").offset().top | 0;

				if (windowTop >= logoTop) {
					$(".splash .logo").css("opacity", "0");
					// $("header.homepage .item.logo img").css("display", "block");
					$("header.homepage .item.logo img").addClass("visible");
					$(".homepage .gallery").css({
						// position: "absolute",
						// top: logoTop
						// position: "absolute"
					})
					$(".homepage .gallery").addClass("scrollable");
				} else {
					$(".splash .logo").css("opacity", "1");
					// $("header.homepage .item.logo img").css("display", "none");
					$("header.homepage .item.logo img").removeClass("visible");
					$(".homepage .gallery").css({
						// position: "fixed",
						// top: "0px"
						// position: "fixed"
					})
					$(".homepage .gallery").removeClass("scrollable");
				}

				var galleryTopPos = $(".homepage .gallery").position();

			} // End of .width()

		} // End of $(".splash").length

	}) // End of .scroll()

	$(window).resize(function() {

		console.log("Resize!");
			
		if ($(window).width() > 960) {

			var galleryTopPos = $(".homepage .gallery").position();

			// if ($(".homepage .gallery").hasClass("scrollable")) {
			// 	$(".homepage .gallery").css({
			// 		position: "fixed",
			// 		top: "0px"
			// 	})
			// }

			// } else {
			// 	$(".homepage .gallery").css({
			// 		top: logoTop
			// 	})
			// }

		} // End of .width()

	}) // End of .resize()

function resizeMasonryItem(item) {
	var grid = document.getElementsByClassName("masonry")[0];
	var rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue("grid-row-gap"));
	var rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue("grid-auto-rows"));
	var rowSpan = Math.ceil((item.querySelector(".masonry-content").getBoundingClientRect().height + rowGap) / (rowHeight + rowGap));

	item.style.gridRowEnd = "span " + rowSpan;
}

function resizeAllMasonryItems() {
  var allItems = document.getElementsByClassName("masonry-item");

  for (var i = 0; i < allItems.length; i ++) {
    resizeMasonryItem(allItems[i]);
  }
}

function waitForImages() {
  var allItems = document.getElementsByClassName("masonry-item");
  for (var i  =0; i < allItems.length; i ++){
    imagesLoaded(allItems[i], function(instance) {
      var item = instance.elements[0];
      resizeMasonryItem(item);
    } );
  }
}

var masonryEvents = ["load", "resize"];
masonryEvents.forEach( function(event) {
  window.addEventListener(event, resizeAllMasonryItems);
} );

waitForImages();

} // End of .init()