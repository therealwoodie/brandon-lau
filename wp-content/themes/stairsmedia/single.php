<?
get_header();
if (have_posts()) : while (have_posts()) :
	the_post(); ?>

	<div class="blog">
		<div class="wpr">
			<h1><? the_title(); ?></h1>
			<p><? the_content(); ?></p>
			<a href="/blog">Back to All Posts</a>
		</div>
	</div>

<? endwhile;
endif;
get_footer(); ?>