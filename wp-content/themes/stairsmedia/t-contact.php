<?
/* Template Name: Contact */
get_header();
if (have_posts()) : while (have_posts()) :
	the_post(); ?>

		<div class="contact">
			<div class="contact--content">
				<div class="contact--details">
					<? the_content(); ?>
				</div>
				<div class="contact--image">
					<? $image = get_field("bg_img"); ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>

			<div class="social">
				<a href="<?= the_field('linkedin'); ?>" target="_blank"><img src="/wp-content/uploads/2019/02/linkedin.png"></a>
				<a href="<?= the_field('instagram'); ?>" target="_blank"><img src="/wp-content/uploads/2019/02/instagram.png"></a>
				<a href="<?= the_field('stairs_media'); ?>" target="_blank"><img src="/wp-content/uploads/2019/02/stairs.png"></a>
			</div>
		</div>

<? endwhile;
endif;
get_footer(); ?>