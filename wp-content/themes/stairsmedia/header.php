<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Hind+Siliguri:400,600,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
	<script type="text/javascript" src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/barba.js/1.0.0/barba.min.js"></script>

	<?php wp_head(); ?>
</head>

<body <? body_class(); ?>>

<!-- Testing -->

	<div id="barba-wrapper">
		<div class="barba-container">

	<!-- <div class="container"> -->

		<!-- <div class="solid-bg"></div> -->

<? $pageTemplate = explode(".", get_page_template_slug()); ?>
<? $currentPage = $pageTemplate[0]; ?>
<? $homepageTemplate = array("t-homepage"); ?>

		<header class="<? if (in_array($currentPage, $homepageTemplate)) { echo "homepage"; } ?>">
			<div class="item blog">
				<a href="/blog">Blog</a>
			</div>
			<div class="item logo">
				<!-- <a href="/">&nbsp;</a> -->
				<a href="/">
					<img src="/wp-content/uploads/2019/02/logo.svg">
				</a>
			</div>
			<div class="item contact">
				<a href="/contact">Contact</a>
			</div>

			<div class="hamburger">
				<span></span>
				<span></span>
			</div>

			<nav>
				<div class="links">
					<a href="/blog">Blog</a>
					<a href="/contact">Contact</a>
				</div>
			</nav>
		</header>

		<!-- <div class="gradient"></div> -->

		<div class="overlay"></div>
