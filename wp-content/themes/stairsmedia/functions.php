<?

function danukimberly_setup() {
	add_theme_support("post-thumbnails");

	add_theme_support("title-tag");

	add_theme_support("automatic-feed-links");

	register_nav_menus(array(
		"header" => __("Header", "stairsmedia"),
		"footer" => __("Footer", "stairsmedia"),
		"responsive" => __("Responsive", "stairsmedia")
	));
}

add_action("after_setup_theme", "danukimberly_setup");

// Load CSS and JS files
function danukimberly_load_scripts() {
	wp_register_style("stylesheet", get_stylesheet_directory_uri()."/dist/stylesheet.css", "", array(), "screen");
	wp_enqueue_style("stylesheet");

	wp_register_style("custom-stylesheet", get_stylesheet_directory_uri()."/assets/css/custom.css", "", array(), "screen");
	wp_enqueue_style("custom-stylesheet");

	wp_register_script("script", get_template_directory_uri()."/dist/main-min.js", array(), "", true);
	wp_enqueue_script("script");
}

add_action("wp_enqueue_scripts", "danukimberly_load_scripts");

// Remove WP Emoji
remove_action("wp_head", "print_emoji_detection_script", 7);
remove_action("wp_print_styles", "print_emoji_styles");

// Hide admin bar
show_admin_bar(false);

// Edit excerpt character length
add_filter("excerpt_length", function($length) {
	return 15;
});

// Edit "read more" after excerptx
function excerpt_more($link) {
	return "";
}

add_filter("excerpt_more", "excerpt_more");

function cc_mime_types($mimes) {
  $mimes["svg"] = "image/svg+xml";
  return $mimes;
}
add_filter("upload_mimes", "cc_mime_types");
?>